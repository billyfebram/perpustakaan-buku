package com.atibusinessgroup.perpustakaan.web.rest;

import com.atibusinessgroup.perpustakaan.BukuApp;
import com.atibusinessgroup.perpustakaan.domain.Buku;
import com.atibusinessgroup.perpustakaan.repository.BukuRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BukuResource} REST controller.
 */
@SpringBootTest(classes = BukuApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class BukuResourceIT {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    @Autowired
    private BukuRepository bukuRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBukuMockMvc;

    private Buku buku;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Buku createEntity(EntityManager em) {
        Buku buku = new Buku()
            .title(DEFAULT_TITLE);
        return buku;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Buku createUpdatedEntity(EntityManager em) {
        Buku buku = new Buku()
            .title(UPDATED_TITLE);
        return buku;
    }

    @BeforeEach
    public void initTest() {
        buku = createEntity(em);
    }

    @Test
    @Transactional
    public void createBuku() throws Exception {
        int databaseSizeBeforeCreate = bukuRepository.findAll().size();
        // Create the Buku
        restBukuMockMvc.perform(post("/api/bukus")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(buku)))
            .andExpect(status().isCreated());

        // Validate the Buku in the database
        List<Buku> bukuList = bukuRepository.findAll();
        assertThat(bukuList).hasSize(databaseSizeBeforeCreate + 1);
        Buku testBuku = bukuList.get(bukuList.size() - 1);
        assertThat(testBuku.getTitle()).isEqualTo(DEFAULT_TITLE);
    }

    @Test
    @Transactional
    public void createBukuWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = bukuRepository.findAll().size();

        // Create the Buku with an existing ID
        buku.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBukuMockMvc.perform(post("/api/bukus")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(buku)))
            .andExpect(status().isBadRequest());

        // Validate the Buku in the database
        List<Buku> bukuList = bukuRepository.findAll();
        assertThat(bukuList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = bukuRepository.findAll().size();
        // set the field null
        buku.setTitle(null);

        // Create the Buku, which fails.


        restBukuMockMvc.perform(post("/api/bukus")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(buku)))
            .andExpect(status().isBadRequest());

        List<Buku> bukuList = bukuRepository.findAll();
        assertThat(bukuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBukus() throws Exception {
        // Initialize the database
        bukuRepository.saveAndFlush(buku);

        // Get all the bukuList
        restBukuMockMvc.perform(get("/api/bukus?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(buku.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE)));
    }
    
    @Test
    @Transactional
    public void getBuku() throws Exception {
        // Initialize the database
        bukuRepository.saveAndFlush(buku);

        // Get the buku
        restBukuMockMvc.perform(get("/api/bukus/{id}", buku.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(buku.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE));
    }
    @Test
    @Transactional
    public void getNonExistingBuku() throws Exception {
        // Get the buku
        restBukuMockMvc.perform(get("/api/bukus/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBuku() throws Exception {
        // Initialize the database
        bukuRepository.saveAndFlush(buku);

        int databaseSizeBeforeUpdate = bukuRepository.findAll().size();

        // Update the buku
        Buku updatedBuku = bukuRepository.findById(buku.getId()).get();
        // Disconnect from session so that the updates on updatedBuku are not directly saved in db
        em.detach(updatedBuku);
        updatedBuku
            .title(UPDATED_TITLE);

        restBukuMockMvc.perform(put("/api/bukus")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedBuku)))
            .andExpect(status().isOk());

        // Validate the Buku in the database
        List<Buku> bukuList = bukuRepository.findAll();
        assertThat(bukuList).hasSize(databaseSizeBeforeUpdate);
        Buku testBuku = bukuList.get(bukuList.size() - 1);
        assertThat(testBuku.getTitle()).isEqualTo(UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void updateNonExistingBuku() throws Exception {
        int databaseSizeBeforeUpdate = bukuRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBukuMockMvc.perform(put("/api/bukus")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(buku)))
            .andExpect(status().isBadRequest());

        // Validate the Buku in the database
        List<Buku> bukuList = bukuRepository.findAll();
        assertThat(bukuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBuku() throws Exception {
        // Initialize the database
        bukuRepository.saveAndFlush(buku);

        int databaseSizeBeforeDelete = bukuRepository.findAll().size();

        // Delete the buku
        restBukuMockMvc.perform(delete("/api/bukus/{id}", buku.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Buku> bukuList = bukuRepository.findAll();
        assertThat(bukuList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
