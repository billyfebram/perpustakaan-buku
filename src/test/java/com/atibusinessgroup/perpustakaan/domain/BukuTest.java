package com.atibusinessgroup.perpustakaan.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.atibusinessgroup.perpustakaan.web.rest.TestUtil;

public class BukuTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Buku.class);
        Buku buku1 = new Buku();
        buku1.setId(1L);
        Buku buku2 = new Buku();
        buku2.setId(buku1.getId());
        assertThat(buku1).isEqualTo(buku2);
        buku2.setId(2L);
        assertThat(buku1).isNotEqualTo(buku2);
        buku1.setId(null);
        assertThat(buku1).isNotEqualTo(buku2);
    }
}
