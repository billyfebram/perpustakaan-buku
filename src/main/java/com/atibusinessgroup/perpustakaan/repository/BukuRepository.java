package com.atibusinessgroup.perpustakaan.repository;

import com.atibusinessgroup.perpustakaan.domain.Buku;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Buku entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BukuRepository extends JpaRepository<Buku, Long> {
}
