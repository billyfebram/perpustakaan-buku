package com.atibusinessgroup.perpustakaan.web.rest;

import com.atibusinessgroup.perpustakaan.domain.Buku;
import com.atibusinessgroup.perpustakaan.repository.BukuRepository;
import com.atibusinessgroup.perpustakaan.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.atibusinessgroup.perpustakaan.domain.Buku}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class BukuResource {

    private final Logger log = LoggerFactory.getLogger(BukuResource.class);

    private static final String ENTITY_NAME = "bukuBuku";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BukuRepository bukuRepository;

    public BukuResource(BukuRepository bukuRepository) {
        this.bukuRepository = bukuRepository;
    }

    /**
     * {@code POST  /bukus} : Create a new buku.
     *
     * @param buku the buku to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new buku, or with status {@code 400 (Bad Request)} if the buku has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/bukus")
    public ResponseEntity<Buku> createBuku(@Valid @RequestBody Buku buku) throws URISyntaxException {
        log.debug("REST request to save Buku : {}", buku);
        if (buku.getId() != null) {
            throw new BadRequestAlertException("A new buku cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Buku result = bukuRepository.save(buku);
        return ResponseEntity.created(new URI("/api/bukus/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /bukus} : Updates an existing buku.
     *
     * @param buku the buku to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated buku,
     * or with status {@code 400 (Bad Request)} if the buku is not valid,
     * or with status {@code 500 (Internal Server Error)} if the buku couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/bukus")
    public ResponseEntity<Buku> updateBuku(@Valid @RequestBody Buku buku) throws URISyntaxException {
        log.debug("REST request to update Buku : {}", buku);
        if (buku.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Buku result = bukuRepository.save(buku);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, buku.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /bukus} : get all the bukus.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of bukus in body.
     */
    @GetMapping("/bukus")
    public ResponseEntity<List<Buku>> getAllBukus(Pageable pageable) {
        log.debug("REST request to get a page of Bukus");
        Page<Buku> page = bukuRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /bukus/:id} : get the "id" buku.
     *
     * @param id the id of the buku to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the buku, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/bukus/{id}")
    public ResponseEntity<Buku> getBuku(@PathVariable Long id) {
        log.debug("REST request to get Buku : {}", id);
        Optional<Buku> buku = bukuRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(buku);
    }

    /**
     * {@code DELETE  /bukus/:id} : delete the "id" buku.
     *
     * @param id the id of the buku to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/bukus/{id}")
    public ResponseEntity<Void> deleteBuku(@PathVariable Long id) {
        log.debug("REST request to delete Buku : {}", id);
        bukuRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
