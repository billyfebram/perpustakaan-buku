/**
 * View Models used by Spring MVC REST controllers.
 */
package com.atibusinessgroup.perpustakaan.web.rest.vm;
